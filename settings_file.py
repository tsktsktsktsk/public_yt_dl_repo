#These can be adjusted
MAIN_PATH = '../yt_dl/' #Absolute path
DOMAIN_URL = 'https://www.youtube.com'
CHANNEL_URL ='/channel/UC_LTlNRmHCaU9wAIiI8ChMQ/playlists' #Mine: /channel/UC_LTlNRmHCaU9wAIiI8ChMQ/playlists 
AUDIO_FORMAT = '--audio-format mp3'
YT_DL_SETTINGS =  " --extract-audio --ignore-errors "
#These are probably only necessary when yt changes:

RECOG_CLASSES_LIST = ['yt-uix-sessionlink', 'yt-uix-tile-link', 'spf-link', 'yt-ui-ellipsis', 'yt-ui-ellipsis-2'] #For recognizing the right sort of href
startpage, likes, profile, uploaden = 'YouTube-startpagina', """Video's die ik leuk vind""",  "hetspookjee", "uploaden"
ILLEGAL_CHANNELS = [startpage, likes, profile, uploaden]

#Some helper operations
appendage = '%(title)s.%(ext)s'
channel_url = DOMAIN_URL + CHANNEL_URL
yt_dl_string_start = "youtube-dl" + YT_DL_SETTINGS + '-o "'
yt_dl_string_mid = appendage + '" "' + DOMAIN_URL

#Test url:
# youtube-dl --extract-audio -o '/home/casper/Desktop/%(title)s.%(ext)s' 'https://www.youtube.com/playlist?list=PLeUnGByKimnWeEOBeNx3ZhSkgE9ZDVYgY'
