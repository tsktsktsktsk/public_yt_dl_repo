import os
import sys
print sys.path
import settings_file
import requests
from bs4 import BeautifulSoup


        
def make_dir(path):
    if os.path.exists(path) == False:
        try:
            os.mkdir(path)
        except OSError:
            "Path to the new directory does not exist yet or a syntax error or something else has gone wrong."
    else:
        print "Folder already exists"
 
def write_file(path, lines):    
    with open(path + '_1_code/yt_dl.sh', 'w') as f:
        for line in lines:
            f.write(line + '\n')  

def open_url(url):
    playlist_html = requests.get(url).content
    soup = BeautifulSoup(playlist_html, 'lxml')
    return soup.find_all('a')
                
def make_folders_and_write_file(soup_all_a, main_path, illegal_pages, youtube_dl_string_start, youtube_dl_string_mid):
    youtube_dl_lines = []
	make_dir(main_path + '_1_code/')
    for a in soup_all_a:
        title = a.get('title')
        if title is not None and all(a.get('class')) == all(settings_file.RECOG_CLASSES_LIST) and title not in illegal_pages :
            direc_name = title
            playlist_url = a.get('href') + '"'
            folder_path = main_path + direc_name + '/'
            bash_file_line = youtube_dl_string_start + folder_path + youtube_dl_string_mid + playlist_url
           
            print "Playlist found: {}. Making folder.".format(direc_name)
            make_dir(folder_path)
            
            youtube_dl_lines.append(bash_file_line)
    
    return youtube_dl_lines
  


print "Requesting webpage: {} for building yt_dl file and directories".format(settings_file.DOMAIN_URL + settings_file.CHANNEL_URL)
all_a_soup = open_url(settings_file.channel_url)  

print "Building directories and yt_dl bash script lines"
youtube_dl_lines = make_folders_and_write_file(all_a_soup, settings_file.MAIN_PATH, 
                                                settings_file.ILLEGAL_CHANNELS, 
                                                settings_file.yt_dl_string_start, settings_file.yt_dl_string_mid)       

print "Writing yt_dl.sh to: {}".format("_1_code" +  settings_file.MAIN_PATH)
write_file(settings_file.MAIN_PATH , youtube_dl_lines)
        



        